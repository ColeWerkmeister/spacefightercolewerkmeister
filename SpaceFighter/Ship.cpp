
#include "Ship.h"


Ship::Ship()
{
	//Sets the starting position for the ship at the start of the game
	SetPosition(0, 0);
	//Determines how big the ships collision radius will be (htibox)
	SetCollisionRadius(10);

	//How fast the ship is able to move
	m_speed = 300;
	//How many hits it takes to destroy a ship
	m_maxHitPoints = 3;
	//A variable to keep track if the ship is invisible after getting hit
	m_isInvulnurable = false;

	//Creates ship
	Initialize();
}

void Ship::Update(const GameTime *pGameTime)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Update(pGameTime);
	}

	GameObject::Update(pGameTime);
}

void Ship::Hit(const float damage)
{
	//If ship is not Invulnurable execute code
	if (!m_isInvulnurable)
	{
		//Reduce ships hitpoints based on the parameter damage
		m_hitPoints -= damage;

		//If hitpoints reaches zero end game
		if (m_hitPoints <= 0)
		{
			GameObject::Deactivate();
		}
	}
}

void Ship::Initialize()
{
	m_hitPoints = m_maxHitPoints;
}

void Ship::FireWeapons(TriggerType type)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Fire(type);
	}
}

void Ship::AttachWeapon(Weapon *pWeapon, Vector2 position)
{
	pWeapon->SetGameObject(this);
	pWeapon->SetOffset(position);
	m_weapons.push_back(pWeapon);
}