
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	//Determines how many hits it takes to destroy a ship
	SetMaxHitPoints(1);
	//Determines how close the enemy ship will need to be to the player ship for it to be considered a collision
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0)
	{
		//Determines how many second has elasped from when the game started
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			//Spawns the enemy ship
			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		//Keeps track of how long the enemy ship has been on screen
		m_activationSeconds += pGameTime->GetTimeElapsed();
		//If the enemy ship how been on the screen for more than 2 second it will deactive the ship
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	//Sets position on where it will first appear on screen
	SetPosition(position);
	//Sets how long it will take before the ship spawns
	m_delaySeconds = delaySeconds;

	//Creates ship on screen
	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}